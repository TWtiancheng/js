import {expect} from 'chai';

import {average, max} from './calculations';

describe('calculations', () => {
    describe('max', () => {
        context('when numbers are undefined', () => {
            it('returns null', () => {
                expect(max(undefined)).to.eql(null);
            });
        });

        context('when numbers are empty', () => {
            it('returns null', () => {
                expect(max([])).to.eql(null);
            });
        });

        context('when numbers are only one number', () => {
            it('returns that number', () => {
                expect(max([1])).to.eql(1);
            });
        });

        context('when numbers have two number', () => {
            it('returns max number', () => {
                expect(max([1,2])).to.eql(2);
            });
        });

        context('when numbers have three number', () => {
            it('returns max number', () => {
                expect(max([1,2,3])).to.eql(3);
            });
        });
    });

    describe('average', () => {
        context('when numbers are undefined', () => {
            it('returns null', () => {
                expect(average(undefined)).to.eql(null);
            });
        });

        context('when numbers are empty', () => {
            it('returns null', () => {
                expect(average([])).to.eql(null);
            });
        });

        context('when numbers only have one number', () => {
            it('returns that number', () => {
                expect(average([1])).to.eql(1);
            });
        });

        context('when numbers have two number', () => {
            it('returns average number', () => {
                expect(average([1,2])).to.eql(1.5);
            });
        });

        context('when numbers have three number', () => {
            it('returns average number', () => {
                expect(average([1,2,3])).to.eql(2);
            });
        });
    });
});
