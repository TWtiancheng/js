
const maxAB = (a,b)=> a>b?a:b;

export const max = (numbers=[]) => {
    if (numbers.length === 1){
        return numbers[0];
    }
    return numbers.length?numbers.reduce(maxAB):null;
};

const sum = (a,b)=>a+b;

export const average = (numbers=[]) => {
    if (numbers.length === 1){
        return numbers[0];
    }
    return numbers.length?numbers.reduce(sum)/numbers.length:null;
};